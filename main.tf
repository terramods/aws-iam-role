provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
  version = "~> 2.35"
}

terraform {
  required_version = ">= 0.12.21"
  backend "s3" {}
}

locals {
  base_name = "${lower(var.owner)}-${lower(var.project)}${var.product != "" ? format("-%s", lower(var.product)) : ""}-${lower(var.environment)}"
  base_tags = {
    Deployer = "Terraform"
    Owner = title(var.owner)
    Project = title(var.project)
  }
}

resource "aws_iam_policy" "tm_custom_iam_policy" {
  count   = length(var.custom_iam_policies)
  name    = "${var.name_forced != "" ? var.name_forced : "${local.base_name}${var.name_suffix != "" ? format("-%s", var.name_suffix) : ""}"}-${var.custom_iam_policies[count.index].aws_resource}-policy"
  policy  = data.aws_iam_policy_document.tm_custom_iam_policy[count.index].json
}

resource "aws_iam_role" "tm_iam_role" {
  name    = "${var.name_forced != "" ? var.name_forced : "${local.base_name}${var.name_suffix != "" ? format("-%s", var.name_suffix) : ""}"}-role"
  assume_role_policy = data.aws_iam_policy_document.tm_assume_role_iam_policy.json
}

resource "aws_iam_role_policy_attachment" "tm_legacy_iam_role_policy_attachment" {
  count      = length(var.legacy_iam_policies)
  role       = aws_iam_role.tm_iam_role.name
  policy_arn = var.legacy_iam_policies[count.index]

  depends_on = [aws_iam_role.tm_iam_role]
}

resource "aws_iam_role_policy_attachment" "tm_custom_iam_role_policy_attachment" {
    count      = length(aws_iam_policy.tm_custom_iam_policy)
    role       = aws_iam_role.tm_iam_role.name
    policy_arn = aws_iam_policy.tm_custom_iam_policy[count.index].arn

    depends_on = [aws_iam_role.tm_iam_role, aws_iam_policy.tm_custom_iam_policy]
}